#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "include/chip8.h"
#include "include/defines.h"

unsigned char font[80] =
{ 
    0xF0, 0x90, 0x90, 0x90, 0xF0, //0
    0x20, 0x60, 0x20, 0x20, 0x70, //1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
    0x90, 0x90, 0xF0, 0x10, 0x10, //4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
    0xF0, 0x10, 0x20, 0x40, 0x40, //7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
    0xF0, 0x90, 0xF0, 0x90, 0x90, //A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
    0xF0, 0x80, 0x80, 0x80, 0xF0, //C
    0xE0, 0x90, 0x90, 0x90, 0xE0, //D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
    0xF0, 0x80, 0xF0, 0x80, 0x80  //F
};

void init(struct chip8 *this){
	// TODO: Initialize registers and memory once at the start of the 
	// program. 
	
	// set program counter to 0x200(program start)
	this->pc = 0x200;

	
	this->opcode = 0; // reset current opcode
	this->index  = 0; // reset index register
	this->sp     = 0; // reset stack pointer

	// clear memory, stack, registers, and display
	memset(this->memory, 0, sizeof(this->memory));	
	memset(this->stack, 0, sizeof(this->stack));
	memset(this->V, 0, sizeof(this->V));
	memset(this->gfx, 0, sizeof(this->gfx));

	// load font
	for(int i = 0; i < 80; ++i){
		this->memory[i] = font[i];
	}		
	
	this->delayTimer = 0; // reset delay timer
	this->soundTimer = 0; // reset sound timer
}

void cycle(struct chip8 *this){
	// TODO: Fetch, decode, and execute opcodes, and update timers

	// fetch the current opcode and increment pc by 2
	this->opcode = this->memory[this->pc] << 8 | this->memory[this->pc + 1];
	this->pc += 2; // increment PC twice to fetch and decode the next opcode

	// decode opcode
	switch(this->opcode & 0xF000){
		case 0x0000:
			switch(this->opcode & 0x000F){
			
				case 0x0000: // opcode 0x00E0(clear screen)
					memset(this->gfx, 0, sizeof(this->gfx)); // clear display
					
					break;
					
				case 0x000E: // opcode 0x00EE(return from subroutine)
					this->sp -= 1;                    // decrement stack to avoid overflow
					this->pc = this->stack[this->sp]; // put the stored address back into pc

					break;	
			}
		break;
		case 0x1000: // opcode 0x1NNN(jump to address NNN)
			this->pc = this->opcode & 0xFFF;
			break; 
			
		case 0x2000: // opcode 0x2NNN(enter subroutine located at NNN)
			this->stack[this->sp] = this->pc; // store the current address into the stack
			this->sp += 1;                    // increment the stack pointer

			this->pc = this->opcode & 0xFFF;  // jump to the subroutine located at NNN
			
			break;
			
		case 0x3000: // opcode 0x3XNN(skip the following instruction if value in register VX equals NN)
			if(this->V[(this->opcode & 0x0F00) >> 8] == (this->opcode & 0x0FF)){
				this->pc += 2;
			}
			break;
			
		case 0x4000: // opcode 0x4XNN(skip the following instruction if value in register VX does not equal NN)
			if(this->V[(this->opcode & 0x0F00) >> 8] != (this->opcode & 0x0FF)){
				this->pc += 2;
			}
			break;

		case 0x5000: // opcode 0x5XY0(skip the following instruciton if value in register VX equals the value in register VY)
			if(this->V[(this->opcode & 0x0F00) >> 8] == this->V[(this->opcode & 0x00F0) >> 4]){
				this->pc += 2;
			}
			break;

		case 0x6000: // opcode 0x6XNN(set VX to NN)
			this->V[(this->opcode & 0x0F00) >> 8] = this->opcode & 0X00FF;
			break;
			
		case 0x7000: // opcode 0x7XNN(add NN to VX)
			this->V[(this->opcode & 0x0F00) >> 8] += this->opcode & 0X00FF;
			// printf("Unknown Opcode: 0x%x\n", this->opcode);
			break;

		case 0x8000: 			
			// printf("Unknown Opcode: 0x%x\n", this->opcode);
			switch(this->opcode & 0x000F){
				case 0x0000:  // opcode 0x8XY0(store the value of VY into VX)
					this->V[(this->opcode & 0x0F00) >> 8] = this->V[(this->opcode & 0x00F0) >> 4];					
					break;
					
				case 0x0001: // opcode 0x8XY1(set VX to VX OR VY)
					this->V[(this->opcode & 0x0F00) >> 8] |= this->V[(this->opcode & 0x00F0) >> 4];
					break;

				case 0x0002: // opcode 0x8XY2(set VX to VX AND VY)
					this->V[(this->opcode & 0x0F00) >> 8] &= this->V[(this->opcode & 0x00F0) >> 4];
					break;

				case 0x0003: // opcode 0x08XY3(set VX to VX XOR VY);
					this->V[(this->opcode & 0x0F00) >> 8] ^= this->V[(this->opcode & 0x00F0) >> 4];
					break;
				case 0x0004: // opcode 0x8XY4 (add the value of register VY to register VX)
					unsigned char result;
					result = this->V[(this->opcode & 0x0F00) >> 8] += this->V[(this->opcode & 0x00F0) >> 4];
					
					if(result >= 255){
						this->V[0x0F] = 1;
					} else{
						this->V[0x0F] = 0;
					}
					break;
				case 0x0005: // opcode 0x8XY5(subtract the value of register VY to VX)
					result = this->V[(this->opcode & 0x0F00) >> 8] -= this->V[(this->opcode & 0x00F0) >> 4];

					if(result <= 0){
						this->V[0x0F] = 1;
					} else{
						this->V[0x0F] = 0;
					}
					break;
				case 0x0006: // opcode 0x8XY6(store the value of register VY shifted right one bit into VX)
					this->V[0xF] = this->V[(this->opcode & 0x0F00) >> 8] & 0x1;
					this->V[(this->opcode & 0x0F00) >> 8] >>= 1;
					break;
				case 0x0007: // opcode 0x8XY7(set register VX to the value of VY minus VX)
					result = this->V[(this->opcode & 0x0F00) >> 8] = this->V[(this->opcode & 0x00F0) >> 4] - this->V[(this->opcode & 0x0F00) >> 8];

					if(result <= 0){
						this->V[0x0F] = 1;
					} else{
						this->V[0x0F] = 0;
					}
					break;
				case 0x000E: // opcode 0x8XYE(store the value of register VY shifted left one bit into VX)
					this->V[0xF] = this->V[(this->opcode & 0x0F00) >> 8] >> 7;
					this->V[(this->opcode & 0x0F00) >> 8] <<= 1;
					break;
			}
			break;

		case 0x9000: // opcode 0x9XY0(skipthe following instruction if the value in register VX does not equal VY)
			if(this->V[(this->opcode & 0x0F00) >> 8] != this->V[(this->opcode & 0x00F0) >> 4]){
				this->pc += 2;
			}
			
			break;

		case 0xA000: // opcode 0xANNN(set index to NNN)
			this->index = this->opcode & 0x0FFF;
			break;

		case 0xB000: // opcode 0xBNNN(jump to address NNN + V0)
			printf("Unknown Opcode: 0x%x\n", this->opcode);
			break;

		case 0xC000: // opcode 0xCXNN(set VX to a random number with the mask of NN)
			printf("Unknown Opcode: 0x%x\n", this->opcode);
			break;

		case 0xD000: // opcode 0xDXYN(draw sprite at VX and VY with height of N pixels)
		{
			unsigned short x = this->V[(this->opcode & 0x0F00) >> 8];
			unsigned short y = this->V[(this->opcode & 0x00F0) >> 4];
			unsigned short height = this->opcode & 0x000F;
			unsigned short pixel;

			// turn pixels off
			this->V[0xF] = 0;

			// get height of the sprite
			for(int yline = 0; yline < height; yline++){
				pixel = this->memory[this->index + yline];

				// get width of the sprite
				for (int xline = 0; xline < 8; xline++){

					// check for pixels, and turn pixels on for each pixel that is supposed to be drawn
					if((pixel & (0x80 >> xline)) != 0){
						if(this->gfx[(x + xline + ((y + yline) * 64))] == 1){
									this->V[0xF] = 1;                                    
						}
						
						this->gfx[x + xline + ((y + yline) * 64)] ^= 1;
					}
				}
			}
			
			// set draw flag to true after drawing operation
			this->drawFlag = true;
		}
			break;
		case 0xF000:
			switch(this->opcode & 0x00FF){
				case 0x0007: // opcode 0xFX07(store the current value of the delay timer in register VX)
					printf("Unknown Opcode: 0x%x\n", this->opcode);
					break;
				case 0x000A: //opcode 0xFX0A(wait for a keypress and store the result in register VX)
					printf("Unknown Opcode: 0x%x\n", this->opcode);
					break;
				case 0x0015: // opcode 0xFX15(set the delay timer to the value currently in register VX)
					printf("Unknown Opcode: 0x%x\n", this->opcode);
					break;
				case 0x0018: // opcode 0xFX18(set the sound timer to the value currently in register VX)
					printf("Unknown Opcode: 0x%x\n", this->opcode);
					break;
				case 0x001E: // opcode 0xFX1E(add the value currently in register VX to the index register)
					this->index += this->V[(this->opcode & 0x0F00) >> 8];
					break;
				case 0x0029: // opvode 0xFX29(set the index register to the address of the sprite data corresponding to the hexidecimal value in VX)
					printf("Unknown Opcode: 0x%x\n", this->opcode);
					break;
				case 0x0033: // opcode 0xFX33(Store the binary-coded decimal equivalent of the value stored in register VX at addresses I, I + 1, and I + 2)
					this->memory[this->index] = this->V[(this->opcode & 0x0F00) >> 8] / 100;
					this->memory[this->index + 1] = (this->V[(this->opcode & 0x0F00) >> 8] / 10) % 10;
					this->memory[this->index + 2] = (this->V[(this->opcode & 0x0F00) >> 8] % 100) % 10;
					break;
				case 0x0055: // opcode 0xFX55(Store the values of registers V0 to VX inclusive in memory starting at address I. I should be set to I + X + 2 after the operation)
					for(int i = 0; i <= ((this->opcode & 0x0F00) >> 8); i++){
						this->memory[this->index + i] = this->V[i];
					}

					this->index += ((this->opcode & 0x0F00) >> 8) + 1;
					break;
				case 0x0065: // opcode 0xFX65(Fill registers V0 to VX inclusive with the values stored in memory starting at address I. I should be set to I + X + 2 after the operation)
					for(int i = 0; i <= ((this->opcode & 0x0F00) >> 8); i++){
						this->V[i] = this->memory[this->index + i];
					}

					this->index += ((this->opcode & 0x0F00) >> 8) + 1;
					break;
				
			}
			break;	
		default:
			printf("Unknown Opcode: 0x%x\n", this->opcode);
	}
	if(this->delayTimer > 0){
		this->delayTimer--;
	}
	
	// if(this->soundTimer == 1){
		// printf("BEEP!\n");
	// }
	this->soundTimer--;
}
 
bool load(struct chip8 *this, const char *filename){
	 // TODO: load rom(file pointer) into memory
	 init(this);
	 printf("Loading: %s\n", filename);

	// open file
	 FILE* pFile = fopen(filename, "rb");
	 if(pFile == NULL){
	 	fputs("File error", stderr);
	 	return false;
	 }

	 // check file size
	 fseek(pFile, 0, SEEK_END);
	 long lSize = ftell(pFile);
	 rewind(pFile);
	 printf("Filesize: %d\n", (int)lSize);

	// allocate memory to contain the whole file
	 char* buffer = (char*)malloc(sizeof(char) * lSize);
	 if(buffer == NULL){
	 	fputs("Memory error", stderr);
	 	return false;
	 }

	 // Copy the file into the buffer
	 size_t result = fread(buffer, 1, lSize, pFile);
	 if(result != lSize){
	 	fputs("Reading error", stderr);
	 	return false;
	 }

	 // copy buffer into chip8 memory
	 if((4096-512) > lSize){
	 	for(int i = 0; i < lSize; i++){
	 		this->memory[i + 512] = buffer[i];
	 	}
	 } else{
	 	printf("Error: ROM too big for memory");
	 }

	 fclose(pFile);
	 free(buffer);

	 return true;
}
