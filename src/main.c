#include "include/defines.h"
#include "include/chip8.h"

#include <SDL2/SDL.h>
#include <stdio.h>

struct chip8 cpu;

SDL_Window* win = NULL;
SDL_Renderer* ren = NULL;

void display(){
	for(int y = 0; y < 32; y++){
		for(int x = 0; x < 64; x++){
			if(cpu.gfx[(y*64) + x] == 0){
				SDL_SetRenderDrawColor(ren, 0, 118, 147, 255);
				SDL_RenderDrawPoint(ren, x, y);
			} else {
				SDL_SetRenderDrawColor(ren, 205, 238, 247, 255);
				SDL_RenderDrawPoint(ren, x, y);
			}
		}
	}
}

void debugDisplay(){
	for(int y = 0; y < 32; y++){
		for(int x = 0; x < 64; x++){
			if(cpu.gfx[(y*64) + x] == 0){
				printf(" ");
			} else {
				printf("x");
			}
		}
	}
}

int main(){
	int scaleFactor = 3 * 5;
	
	if(SDL_Init(SDL_INIT_VIDEO)){
		puts("SDL2 could not initialize");
	}
	else {
		win = SDL_CreateWindow("Squeaky8", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 64 * scaleFactor, 32 * scaleFactor, SDL_WINDOW_RESIZABLE);
		ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

		// set renderer native resolutioin to handle letterboxing and enable vsync
		SDL_RenderSetLogicalSize(ren, 64, 32);
		SDL_RenderSetVSync(ren, 1);
		// SDL_Texture* texture = SDL_CreateTexture(ren, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_TARGET, 64, 32);
		// SDL_SetRenderTarget(ren, texture);
	}
	
	init(&cpu);
	
	// load test rom. this will be commented out in the final release
	load(&cpu, "src/tests/3-corax+.ch8");

	// for(int i = 0; i < MEM_COUNT; i++){
		// printf("%i ", cpu.memory[i]);
	// }
	
	
	SDL_Event e;
	// keep the window open until the user closes the application
	while(1){
		while(SDL_PollEvent(&e)){
			if(e.type == SDL_QUIT){
				exit(0);		
			}
		}

		// run emulation cycle once per frame
		cycle(&cpu);

		
		if(cpu.drawFlag){
			// draw video output to a texture
			SDL_RenderClear(ren);
			display();
			// debugDisplay();
			SDL_RenderPresent(ren);

			cpu.drawFlag = false;						
		}
		
	}
		
}
