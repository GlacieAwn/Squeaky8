#pragma once

#define SCREEN_WIDTH 64;
#define SCREEN_HEIGHT 32;

// define general system constants
#define MEM_COUNT  4096 // system memory amount
#define REG_COUNT  16   // register count
