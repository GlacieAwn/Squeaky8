#pragma once 

#include <stdbool.h>


struct chip8{

	// instructions, memory amount in bytes, and register count
	unsigned short opcode;
	unsigned char memory[4096];
	unsigned char V[16];

	// index and program counter
	unsigned short index;
	unsigned short pc;

	// timer registers
	unsigned char delayTimer;
	unsigned char soundTimer;

	// stack and stack pointer
	unsigned short stack[16];
	unsigned short sp;

	// current state of the keypad
	unsigned char key[16];

	// graphics ram defnintion
	unsigned char gfx[64 * 32];

	// draw flag.
	bool drawFlag;
};

void init(struct chip8 *this);
void cycle(struct chip8 *this);
bool load(struct chip8 *this, const char *filename);
