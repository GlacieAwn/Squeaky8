#!/usr/bin/env -S make -f
.SUFFIX: # disable suffix rules

bin=Squeaky8
src ::= $(wildcard src/*.c)
obj ::= $(patsubst src/%.c,out/obj/%.o,${src})
dep ::= $(patsubst src/%.c,out/dep/%.d,${src})
out ::= out/$(bin)

PROFILE ::= /tmp/pgo
CFLAGS ?= 
LDFLAGS ?= 
LDLIBS ::= -lSDL2 -lSDL2_image

.PHONY: dev release clean purge

dev: CFLAGS += -Og -Wall -Wpedantic -Wextra -Wabi 
dev: LDFLAGS += --coverage
dev: ${out}

release: CFLAGS += -Ofast -flto 
release: ${out}

all: dev release

clean:
	${RM} -rf out

${out}: ${obj}
	@mkdir -p ${@D}
	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $^ ${LDLIBS}
	./$@

out/dep/%.d: src/%.c
	@mkdir -p ${@D}
	${CC} ${CPPFLAGS} -MM -MG -MF $@ -MT "${@:out/dep/%.d=out/obj/%.o} $@" $<

out/obj/%.o: src/%.c
	@mkdir -p ${@D}
	${CC} ${CFLAGS} ${CPPFLAGS} -c -o $@ $<

include ${dep}
